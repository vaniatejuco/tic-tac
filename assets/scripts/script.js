
let turn = document.getElementById("turn");
// boxes => all boxes
// X_or_O => to set X or O into the box
let boxes = document.querySelectorAll("#main div");
let X_or_O = 0;

function selectWinnerBoxes(b1,b2,b3){
    b1.classList.add("win");
    b2.classList.add("win");
    b3.classList.add("win");
    turn.innerHTML = "Winner: " + b1.innerHTML ;
    turn.style.fontSize = "40px";
}

getWinner = () => {
  let box = document.querySelectorAll('.box');
  let win_combos = [
  [0,1,2],
  [3,4,5],
  [6,7,8],
  [0,3,6],
  [1,4,7],
  [2,5,8],
  [0,4,8],
  [2,4,6],
];

for (count = 0; count <= 7; count++) {
    if (box[win_combos[count][0]].innerHTML !== "" && box[win_combos[count][0]].innerHTML === box[win_combos[count][1]].innerHTML && box[win_combos[count][0]].innerHTML === box[win_combos[count][2]].innerHTML){
        selectWinnerBoxes(box[win_combos[count][0]],box[win_combos[count][1]],box[win_combos[count][2]]);
    }    

  }

}


// set event onclick
for(i = 0; i < boxes.length; i++){
    boxes[i].onclick = function(){
        // not allow to change the value of the box
        if(this.innerHTML !== "X" && this.innerHTML !== "O"){
        if(X_or_O%2 === 0){
           console.log(X_or_O);
           this.innerHTML = `<img src="assets/images/1.png" height="150" width="150"></img>`; 
           turn.innerHTML = "Flower's Turn Now";
           getWinner();
           X_or_O += 1;
           
        }else{
            console.log(X_or_O);
           this.innerHTML = '<img src="assets/images/2.png" height="150" width="150"></img>';
           turn.innerHTML = "Butterfly's Turn Now";
           getWinner();
           X_or_O += 1;  
        }
    }
        
    };
}

function replay(){
    
    for(i = 0; i < boxes.length; i++){
        boxes[i].classList.remove("win");
        boxes[i].innerHTML = "";
        turn.innerHTML = "Play";
        turn.style.fontSize = "25px";
        
    }
    
}